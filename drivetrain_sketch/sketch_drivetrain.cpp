// MIT License
//
// Copyright (c) 2018 Jelle Spijker
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include <chrono/physics/ChSystemNSC.h>
#include <chrono/physics/ChSystem.h>
#include <chrono/physics/ChShaft.h>
#include <chrono/physics/ChShaftsPlanetary.h>
#include <chrono/physics/ChShaftsGear.h>
#include <chrono/physics/ChShaftsTorsionSpring.h>
#include <chrono_postprocess/ChGnuPlot.h>
#include <chrono_vehicle/powertrain/ShaftsPowertrain.h>
#include <chrono_vehicle/ChVehicleModelData.h>
#include <chrono_vehicle/ChConfigVehicle.h>
#include <chrono_vehicle/ChChassis.h>
#include <chrono/physics/ChBody.h>

#include <vector>
#include <iostream>
#include <cmath>

using namespace chrono;
using namespace chrono::vehicle;
using namespace chrono::postprocess;

int main(int argc,
         char *argv[]) {
  GetLog() << "powertrain tutorial\n\n";
  const std::string powertrain_file{ "/generic/powertrain/ShaftsPowertrain.json"};

  ChSystem *my_system = new ChSystemNSC();

  auto my_chassis = std::make_shared<ChBody>();
  my_chassis->SetBodyFixed(true);
  my_system->Add(my_chassis);

  auto shaft_in = std::make_shared<ChShaft>();
  shaft_in->SetInertia(100);
  my_system->Add(shaft_in);

  auto my_powertrain = std::make_shared<ShaftsPowertrain>(vehicle::GetDataFile(powertrain_file));

  std::vector<std::shared_ptr<ChShaft> > left_shafts;
  std::vector<std::shared_ptr<ChShaft> > right_shafts;

  double shaft_inertias[2]{100., 25.};

  for (uint32_t i = 0; i < 2; i++) {
    auto shaft = std::make_shared<ChShaft>();
    shaft->SetShaftFixed(false);
    shaft->SetInertia(shaft_inertias[i]);
    left_shafts.push_back(shaft);
    my_system->Add(shaft);
  }

  for (uint32_t i = 0; i < 2; i++) {
    auto shaft = std::make_shared<ChShaft>();
    shaft->SetShaftFixed(false);
    shaft->SetInertia(shaft_inertias[i]);
    right_shafts.push_back(shaft);
    my_system->Add(shaft);
  }

  auto my_shafts_planetary = std::make_shared<ChShaftsPlanetary>();
  my_shafts_planetary->SetTransmissionRatios(1.0, -0.5, -0.5);
  my_system->Add(my_shafts_planetary);

  auto my_gear_left = std::make_shared<ChShaftsGear>();
  my_gear_left->SetTransmissionRatio(0.5);
  my_system->Add(my_gear_left);

  auto my_gear_right = std::make_shared<ChShaftsGear>();
  my_gear_right->SetTransmissionRatio(0.5);
  my_system->Add(my_gear_right);

  GetLog() << "The system hierarchy:\n";
  my_system->ShowHierarchy(GetLog());

  const double totaltime = 500;
  const double timestep = 1. / 100.;
  const uint32_t totalsteps = static_cast<uint32_t>(totaltime / timestep) + 1;
  ChVectorDynamic<> mx(totalsteps);
  ChVectorDynamic<> my_in(totalsteps);
  ChVectorDynamic<> my_left_0(totalsteps);
  ChVectorDynamic<> my_left_1(totalsteps);

  my_powertrain->Initialize(my_chassis, shaft_in);
  my_shafts_planetary->Initialize(shaft_in, left_shafts[0], right_shafts[0]);
  my_gear_left->Initialize(left_shafts[0], left_shafts[1]);
  my_gear_right->Initialize(right_shafts[0], right_shafts[1]);

  double chronoTime = 0;
  uint32_t i = 0;
  while (chronoTime < totaltime) {
    chronoTime += timestep;
    my_powertrain->Synchronize(chronoTime, chronoTime / totaltime, shaft_in->GetPos_dt());
    my_powertrain->Advance(timestep);
    my_system->DoStepDynamics(timestep);
    mx(i) = chronoTime;
    my_in(i) = my_powertrain->GetMotorSpeed();
    my_left_0(i) = left_shafts[0]->GetPos_dt();
    my_left_1(i) = left_shafts[1]->GetPos_dt();
    i++;
  }

  ChGnuPlot mplot("__tmp_gnuplot.gpl");
  mplot.SetGrid();
  mplot.Plot(mx, my_in, "IN");
  mplot.Plot(mx, my_left_0, "Left 0");
  mplot.Plot(mx, my_left_1, "Left 1");

  return 0;
}
