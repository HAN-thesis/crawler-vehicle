SET(SKETCHES
        sketch_vehicle)

# TODO use find_package(Boost)
include_directories(Boost_INCLUDE_DIRS)


FOREACH (SKETCH ${SKETCHES})
    MESSAGE(STATUS "... add ${SKETCH}")

    INCLUDE_DIRECTORIES(${CMAKE_CURRENT_LIST_DIR})

    ADD_EXECUTABLE(${SKETCH} ${SKETCH}.cpp ${MODEL_FILES})
    SOURCE_GROUP("" FILES ${SKETCH}.cpp)

    SET_TARGET_PROPERTIES(${SKETCH} PROPERTIES
            COMPILE_FLAGS "${CH_CXX_FLAGS}"
            LINK_FLAGS "${CH_LINKERFLAG_EXE}")

    TARGET_LINK_LIBRARIES(${SKETCH}
            ChronoEngine
            ChronoEngine_vehicle
            ChronoModels_vehicle
            ChronoEngine_postprocess)
    ADD_DEPENDENCIES(${SKETCH}
            ChronoEngine
            ChronoEngine_postprocess)
    INSTALL(TARGETS ${SKETCH} DESTINATION ${CH_INSTALL_DEMO})
ENDFOREACH ()